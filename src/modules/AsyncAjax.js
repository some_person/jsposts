'use strict';

export class AsyncAjax {
    async fetchAjax(query) {
        return await fetch(query)
            .then(result => result.json());
    }
}