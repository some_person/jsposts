export class DOM {
    constructorDomTree(html, parentEl) {
        html.forEach(item => {//jq
            let el = this.createElement(item.domEl, item.class, item.hasOwnProperty('innerText') ? item.innerText : '', '', parentEl);

            if (item.domEl === 'a') {
                $(el).attr('href', item.href);
                $(el).attr('data-post-id', item.dataPostId);
            }

            if (item.hasOwnProperty('childrenElems')) {
                this.constructorDomTree(item.childrenElems, el);
            }
        });
    }


    createElement(elemTag, elemClass = '', elemText = '', elemId = '', elemParent) {
        let el = document.createElement(elemTag);//jq
        $(el).addClass(elemClass);
        if (elemText) {
            $(el).text(elemText)
        }
        if (elemId) {
            $(el).attr('id', elemId);
        }
        $(elemParent).append(el);
        return el;
    }

    handleShowModal(title) {
        let modal = $('.modal');//jq
        let modalBlock = $('.modal__content');
        let modalWrapper = $('.modal__wrapper');
        // let closeModal = $('.modal__close');

        // modalBlock.classList.add('active');
        modal.addClass('active');
        modalBlock.addClass('active');

        // modal.classList.add('active');
        $('body').addClass('locked');
        $('.modal .modal__title').text(title);
        modalWrapper.text('Wait...');
        modal.on('click', '.modal__close', function () {
            modalBlock.removeClass('active');
            modal.removeClass('active');
            $('body').removeClass('locked');
        });
        // closeModal.onclick = () => {
        //     modalBlock.classList.remove('active');
        //     modal.classList.remove('active');
        //     document.body.className = '';
        // };
        return modalWrapper;
    }

    errorHandler(errorMessage, errorParentElem, errorClass) {
        this.createElement('div', errorClass, errorMessage, '', errorParentElem);
    }
}