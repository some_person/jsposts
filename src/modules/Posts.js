import {AsyncAjax as AsyncAjax} from './AsyncAjax.js';
import {DOM as DOM} from './DOM.js';
import {Comments as Comments} from './Comments.js';

export class Posts {
    constructor() {
        this.dom = new DOM();
        this.ajax = new AsyncAjax();
    }

    async getAllPosts() {
        let posts = await this.ajax.fetchAjax("https://jsonplaceholder.typicode.com/posts");

        let users = await this.ajax.fetchAjax("https://jsonplaceholder.typicode.com/users/");
        posts = posts.map((post) => {
            users.forEach(user => {
                if (post.userId === user.id){
                    post.user = user;
                }
            });
            return post;
        });

        // console.log(posts);
        return posts;
    }

    createPosts(post) {
        let dom = this.dom;
        let html = [
            {
                'domEl': 'div',
                'class': 'post',
                'childrenElems': [
                    {
                        'domEl': 'div',
                        'class': 'wrapper',
                        'childrenElems': [
                            {
                                'domEl': 'div',
                                'class': 'post__header',
                                'childrenElems': [
                                    {
                                        'domEl': 'div',
                                        'class': 'post__id',
                                        'innerText': `#${post.id}`
                                    },

                                    {
                                        'domEl': 'h4',
                                        'class': 'post__title',
                                        'innerText': post.title
                                    },

                                    {
                                        'domEl': 'div',
                                        'class': 'post__user',
                                        'innerText': post.user.username
                                    },
                                ]
                            },
                            {
                                'domEl': 'p',
                                'class': 'post__body',
                                'innerText': post.body
                            },

                            {
                                'domEl': 'a',
                                'class': 'post__show-comments',
                                'innerText': 'Show Comments!',
                                'href': '#!',
                                'dataPostId': post.id
                            }
                        ]
                    },
                ]
            },
        ];
        let body = document.body;
        dom.constructorDomTree(html, body);

        $('.post').on('click', '.post__show-comments', function(e) {
            e.preventDefault();
            let postId = $(this).attr('data-post-id');
            let commentsWrapper = dom.handleShowModal(`Comments to post #${postId}`);

            let comments = new Comments(postId);
            comments.getComments()
                .then(items => {
                    commentsWrapper.text('');
                    items.forEach(item => comments.createCommentsModalBlock(item));
                    return items;
                }).catch(error => dom.errorHandler(error, commentsWrapper, 'error modal__error'));

        });
        // showCommentsButton.forEach(item => {
        //     item.onclick = async function (e) {//jq
        //         e.preventDefault();
        //         let postId = this.getAttribute('data-post-id');
        //         let commentsWrapper = dom.handleShowModal(`Comments to post #${postId}`);
        //
        //         let comments = new Comments(postId);
        //         comments.getComments()
        //             .then(items => {
        //                 commentsWrapper.innerHTML = '';
        //                 items.forEach(item => comments.createCommentsModalBlock(item));
        //                 return items;
        //             }).catch(error => dom.errorHandler(error, commentsWrapper, 'error modal__error'));
        //     }
        // });
    }

    async getUserPosts(inputVal, input) {
        let regexpName = /^[a-z]{1,}[ \-\.\_]?[a-z]+?$/ig;
        if (inputVal !== '' && !isNaN(+inputVal)) {
            let postsWrapper = this.dom.handleShowModal(`Posts by user #${inputVal}`);
            await this.getPostsByUserId(inputVal)
                .then(posts => {
                    if (posts instanceof Error) throw posts;

                    postsWrapper.text('');
                    posts.forEach(post => this.createPostsModalBlock(post));
                    return posts;
                })
                .catch(error => this.dom.errorHandler(error, postsWrapper, 'error modal__error'));
        }else if(inputVal !== '' && inputVal.match(regexpName)) {
            let postsWrapper = this.dom.handleShowModal(`Posts by ${inputVal}`);
            await this.getPostsByUserName(inputVal)
                .then(posts => {
                    if (posts instanceof Error) throw posts;

                    postsWrapper.text('');
                    posts.forEach(post => this.createPostsModalBlock(post));
                    console.log(posts);
                    return posts;
                })
                .catch(error => this.dom.errorHandler(error, postsWrapper, 'error modal__error'));

        }else {
            $(input).next('.error').addClass('active');//jq
            input.nextElementSibling.innerText = 'Error!';
            // console.error('error!');
        }
    }

    async getPostsByUserId(value) {
        return await this.ajax.fetchAjax(`https://jsonplaceholder.typicode.com/posts?userId=${value}`)
            .then(posts => posts.length ? posts : new Error(`No user with id "${value}"`))
            .catch(error => error);
    }

    async getPostsByUserName(value) {
        return await this.ajax.fetchAjax(`https://jsonplaceholder.typicode.com/users`)
            .then(user => user.filter(item => item.username === value))
            .then(async user => user.length ? await this.getPostsByUserId(user[0].id) : new Error(`No user with name "${value}"`))
            .catch(error => error);

    }

    createPostsModalBlock(post) {
        let html = [
            {
                'domEl': 'div',
                'class': 'comments__item comment',
                'childrenElems': [
                    {
                        'domEl': 'div',
                        'class': 'post__header',
                        'childrenElems': [
                            {
                                'domEl': 'div',
                                'class': 'post__id',
                                'innerText': `#${post.id}`
                            },

                            {
                                'domEl': 'h5',
                                'class': 'post__title',
                                'innerText': post.title
                            }
                        ]
                    },
                    {
                        'domEl': 'p',
                        'class': 'post__body',
                        'innerText': post.body
                    },
                ]
            }

        ];

        this.dom.constructorDomTree(html, $('.modal__wrapper'));
    }
}