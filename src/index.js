import './fontAwesome/all.min.js';
import './scss/style.scss';

import {Posts} from "./modules/Posts";

let posts = new Posts();
posts.getAllPosts()
    .then(items => items.forEach(item => posts.createPosts(item)));
handleSearchUserPosts();

function handleSearchUserPosts() {
    $('.header').on('click', '.form-search__btn',(e) => {
        e.preventDefault();
        let input = window['form-search__input'];
        let inputVal = input.value.trim();
        input.nextElementSibling.classList.remove('active');
        return posts.getUserPosts(inputVal, input);
    });
}
